<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

#  Kube Service
* [Service Overview](https://dashboards.gitlab.net/d/kube-main/kube-overview)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22kube%22%2C%20tier%3D%22inf%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Kubernetes"

## Logging

* [Stackdriver](https://cloudlogging.app.goo.gl/81mKjkvq3BfTUwRN9)

## Troubleshooting Pointers

* [../elastic/kibana.md](../elastic/kibana.md)
* [../git/gitlab-review-app-certs.md](../git/gitlab-review-app-certs.md)
* [../kas/kubernetes-agent-basic-troubleshooting.md](../kas/kubernetes-agent-basic-troubleshooting.md)
* [k8s-oncall-setup.md](k8s-oncall-setup.md)
* [kubernetes.md](kubernetes.md)
* [../license/license-gitlab-com.md](../license/license-gitlab-com.md)
* [../version/version-gitlab-com.md](../version/version-gitlab-com.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
